# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'parsbite'
  s.version = '0.1.0'
  s.summary = 'Nothing really'
  s.description = File.read(File.join(File.dirname(__FILE__), 'README.md'))
  s.authors = ['Peter Pentchev']
  s.email = 'roam@ringlet.net'
  s.files = [
    'lib/parsbite.rb'
  ]
  s.executables = [
    'parsbite'
  ]
  s.homepage = 'https://devel.ringlet.net/'
  s.license = 'BSD-2-Clause'
  s.metadata = {
    'rubygems_mfa_required' => 'true'
  }
  s.required_ruby_version = '>= 3.0'
end
