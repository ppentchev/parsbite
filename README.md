# Demonstrate my difficulties with the parsby gem

So here's what happens here:

    $ rm -rf env-ruby
    $ git status --short --ignored  # make sure there are no extraneous files
    $ ruby --version
    ruby 3.0.4p208 (2022-04-12 revision 3fa771dded) [x86_64-linux-gnu]
    $ bundle install
    [...output snipped...]
    $ bundle exec -- bin/parsbite
    /home/roam/lang/ruby/misc/parsbite/env-ruby/lib/ruby/3.0.0/gems/parsby-1.1.1/lib/parsby.rb:377:in `<class:Parsby>': uninitialized constant Parsby::StringIO (NameError)
        from /home/roam/lang/ruby/misc/parsbite/env-ruby/lib/ruby/3.0.0/gems/parsby-1.1.1/lib/parsby.rb:4:in `<top (required)>'
        from /home/roam/lang/ruby/misc/parsbite/lib/parsbite.rb:29:in `require'
        from /home/roam/lang/ruby/misc/parsbite/lib/parsbite.rb:29:in `<module:ParsBite>'
        from /home/roam/lang/ruby/misc/parsbite/lib/parsbite.rb:28:in `<top (required)>'
        from bin/parsbite:30:in `require'
        from bin/parsbite:30:in `<module:Main>'
        from bin/parsbite:29:in `<main>'
